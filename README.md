Scenic hot air balloon flight over the Sonoran Desert viewing 1000's of species of flora, fauna, and wildlife. At altitude touring the majestic views of Phoenix Arizona. At the end of your flight, celebrate with a traditional post flight champagne toast and gourmet breakfast of hors d'oeuvres.

Address: 2136 W Melinda Ln, Phoenix, AZ 85027, USA

Phone: 602-402-8041
